public class Ginger extends Condiment {
    private Beverage beverage;

    public Ginger(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return String.format("%s, ginger", beverage.getDescription());
    }

    @Override
    public double cost() {
        return beverage.cost() + 0.10;
    }
}
