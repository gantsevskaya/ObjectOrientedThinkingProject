public class Macchiato extends Beverage {
    public Macchiato() {
        description = "Macchiato";
    }

    @Override
    public double cost() {
        return 0.9;
    }
}
