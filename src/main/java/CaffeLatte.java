public class CaffeLatte extends Beverage {
    public CaffeLatte() {
        description = "Café Latte";
    }

    public double cost() {
        return 0.9;
    }
}
