public class Cinnamon extends Condiment {
    private Beverage beverage;

    public Cinnamon(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return String.format("%s, cinnamon", beverage.getDescription());
    }

    @Override
    public double cost() {
        return beverage.cost() + 0.1;
    }
}
