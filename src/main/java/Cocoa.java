public class Cocoa extends Beverage {
    public Cocoa() {
        description = "Cocoa";
    }

    public double cost() {
        return 0.5;
    }
}