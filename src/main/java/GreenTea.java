public class GreenTea extends Beverage {
    public GreenTea() {
        description = "Green tea";
    }

    public double cost() {
        return 0.5;
    }
}

