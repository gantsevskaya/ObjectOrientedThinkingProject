public class IcedCoffee extends Beverage {
    public IcedCoffee() {
        description = "Iced Coffee";
    }

    @Override
    public double cost() {
        return 0.9;
    }
}
