public class Vanilla extends Condiment {
    private Beverage beverage;

    public Vanilla(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return String.format("%s, vanilla", beverage.getDescription());
    }

    @Override
    public double cost() {
        return beverage.cost() + 0.05;
    }
}