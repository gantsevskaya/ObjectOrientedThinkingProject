public class FlatWhite extends Beverage {
    public FlatWhite() {
        description = "Flat White";
    }

    @Override
    public double cost() {
        return 0.9;
    }
}
