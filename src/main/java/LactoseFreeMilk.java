public class LactoseFreeMilk extends Condiment {
    private Beverage beverage;

    public LactoseFreeMilk(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return String.format("%s, lactose-free milk", beverage.getDescription());
    }

    @Override
    public double cost() {
        return beverage.cost() + 0.1;
    }
}
