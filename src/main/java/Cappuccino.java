public class Cappuccino extends Beverage {
    public Cappuccino() {
        description = "Cappuccino";
    }

    @Override
    public double cost() {
        return 0.9;
    }
}
