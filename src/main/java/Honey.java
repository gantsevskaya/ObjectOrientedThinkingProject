public class Honey extends Condiment {
    private Beverage beverage;

    public Honey(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return String.format("%s, honey", beverage.getDescription());
    }

    @Override
    public double cost() {
        return beverage.cost() + 0.05;
    }
}
