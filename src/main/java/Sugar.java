public class Sugar extends Condiment {
    private Beverage beverage;

    public Sugar(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return String.format("%s, sugar", beverage.getDescription());
    }

    @Override
    public double cost() {
        return beverage.cost() + 0.10;
    }
}
