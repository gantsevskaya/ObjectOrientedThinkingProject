public class PrintTotal {

    public PrintTotal(Beverage beverage) {
    }

    public static void printTotal(Beverage beverage) {
        System.out.println(String.format("%s. Total: $%.2f. Enjoy your drink!", beverage.getDescription(), beverage.cost()));
    }
}
