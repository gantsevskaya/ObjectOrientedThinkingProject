public class CoffeeMachine {
    public static void main(String[] args) {
        Beverage client1 = new CaffeLatte();
        client1 = new Cinnamon(client1);
        client1 = new Sugar(client1);
        PrintTotal.printTotal(client1);
        Beverage client2 = new BlackTea();
        client2 = new Ginger(client2);
        PrintTotal.printTotal(client2);
        Beverage client3 = new Cocoa();
        client3 = new Sugar(client3);
        client3 = new LactoseFreeMilk(client3);
        PrintTotal.printTotal(client3);
    }
}
