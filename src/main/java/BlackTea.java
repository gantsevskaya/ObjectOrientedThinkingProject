public class BlackTea extends Beverage {
    public BlackTea() {
        description = "Black tea";
    }

    public double cost() {
        return 0.5;
    }
}

